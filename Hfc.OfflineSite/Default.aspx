﻿<%@ Page Title="Home Page" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Hfc.OfflineSite._Default" %>
<!DOCTYPE html>
<html>
<head>
    <title>Maintenance Notice - Site is temporarily unavailable due to maintenance</title>
    <style>
        body {
            font-family: "Roboto", Arial, sans-serif;
            margin: 2em 2em;
            font-size: 1.6em;
        }
    </style>
</head>
<body>
<article>
    <h1>Site is temporarily unavailable due to maintenance</h1>
    
    <p>We expect to have the site back up within 4 hours.</p>
</article>
<br /><hr /><br />
</body>
</html> 
 

