﻿using System;
using System.Configuration;
using System.Web.UI;

namespace Hfc.OfflineSite
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var returnTime = ConfigurationManager.AppSettings["returnTime"];

            if (!string.IsNullOrWhiteSpace(returnTime))
            {
                Response.Headers["Retry-After"] = returnTime;
            }

            Response.StatusCode = 503;
        }
    }
}